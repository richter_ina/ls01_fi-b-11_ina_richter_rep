//import Captain;

public class TestCaptain {

	public static void main(String[] args) {
		// Erzeugen der Objekte
		Captain cap1 = new Captain("Jean Luc", "Picard", 4500.0, 2364);
		Captain cap2 = new Captain("Azetbur", "Gorkon", 0.0, 2373);
		Captain cap3 = new Captain("Maike", "Wicht",  1000000.0, 420);
		
		Captain cap4 = new Captain("Marcus", "Drechsler");
		Captain cap5 = new Captain("William", "Sir");
		/*
		 * TODO: 9. Erzeugen Sie ein zusaetzliches Objekt cap3 und geben Sie es auch auf
		 * der Konsole aus, die Attributwerte denken Sie sich aus.
		 */

		/*
		 * TODO: 10. Erzeugen Sie zwei zusaetzliche Objekte cap4 und cap5 mit dem
		 * Konstruktor, der den Namen und Vornamen initialisiert, die Attributwerte
		 * denken Sie sich aus.
		 */

		// Setzen der Attribute
		/*
		 * TODO: 11. Fuegen Sie cap4 und cap5 jeweils ein Gehalt hinzu, die
		 * Attributwerte denken Sie sich aus. Geben Sie cap4 und cap5 auch auf dem
		 * Bildschirm aus.
		 */
		cap5.setCaptainYears(1994);
		cap5.setSalary(2700.0);
		cap4.setCaptainYears(3);
		cap4.setSalary(3);
		
		// Bildschirmausgabe
		System.out.println("Name: " + cap1.getName());
		System.out.println("Vorname: " + cap1.getSurname());
		System.out.println("Kapit�n seit: " + cap1.getCaptainYears());
		System.out.println("Gehalt: " + cap1.getSalary() + " F�derationsdukaten");
		System.out.println("Vollname: " + cap1.vollname(cap1.getName(), cap1.getSurname()));
		System.out.println("\nName: " + cap2.getSurname());
		System.out.println("Vorname: " + cap2.getName());
		System.out.println("Kapit�n seit: " + cap2.getCaptainYears());
		System.out.println("Gehalt: " + cap2.getSalary() + " Darsek");
		System.out.println("Vollname: " + cap2.vollname(cap2.getName(), cap2.getSurname()));
		System.out.println(cap3.toString());
		System.out.println(cap4.toString());
		System.out.println(cap5.toString());
	}

}