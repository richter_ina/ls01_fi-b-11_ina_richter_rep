
public class Captain {

	// Attribute
	private String surname;
	private int captainYears;
	private double gehalt;
	private String name;
	// TODO: 3. Fuegen Sie in der Klasse 'Captain' das Attribut 'name' hinzu und
	// implementieren Sie die entsprechenden get- und set-Methoden.

	// Konstruktoren
	public Captain(String name, String surname) {
		this.name = name;
		this.surname = surname;
	}
	public Captain(String surname, String name, double gehalt, int captainYears) {
		this.surname = surname;
		this.name = name;
		this.captainYears = captainYears;
		this.gehalt = gehalt;
	}
	// TODO: 4. Implementieren Sie einen Konstruktor, der die Attribute surname
	// und name initialisiert.
	// TODO: 5. Implementieren Sie einen Konstruktor, der alle Attribute
	// initialisiert.

	// Verwaltungsmethoden
	public void setName(String name) {
		this.name = name;
	}
	public String getName() {
		return this.name;
	}
	
	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSurname() {
		return this.surname;
	}

	public void setSalary(double gehalt) {
		// TODO: 1. Implementieren Sie die entsprechende set-Methode.
		// Ber�cksichtigen Sie, dass das Gehalt nicht negativ sein darf.
		if(gehalt >= 0) {
			this.gehalt = gehalt;
		}
		else {
			System.out.println("Das wäre Sklavenarbeit!");
		}
	}

	public double getSalary() {
		// TODO: 2. Implementieren Sie die entsprechende get-Methode.
		return this.gehalt;
	}

	// TODO: 6. Implementieren Sie die set-Methode und die get-Methode f�r
	// captainYears.
	// Ber�cksichtigen Sie, dass das Jahr nach Christus sein soll.
	
	public void setCaptainYears(int captainYears) {
		if (captainYears >= 0) {
			this.captainYears = captainYears;
		}
		else {
			System.out.println("Bist du Jesus, oder was?");
		}
	}
	public int getCaptainYears() {
		return this.captainYears;
	}	
	// Weitere Methoden
	
	// TODO: 7. Implementieren Sie eine Methode 'vollname', die den vollen Namen
	// (Vor- und Nachname) als string zur�ckgibt.
	public String vollname(String name, String surname) {
		String vollname = name+surname;
		return vollname;
	}

	public String toString() {  // overriding the toString() method

		// TODO: 8. Implementieren Sie die toString() Methode.
		String string = "Name: "+this.name+"\nSurname: "+this.surname+"\nYears as Captaiin: "+this.captainYears+"\nGehalt: "+this.gehalt+"\n";
		return string;
	}

}