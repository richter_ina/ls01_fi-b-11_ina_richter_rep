import java.util.ArrayList;
import java.util.Scanner;

public class ArrayLists {

	public static void main(String[] args) {
	/*1. Erzeugen einer Liste vom Typ int, die mit 20 Zufallszahlen zwischen 1 und 9initialisiert wird. DONE
	2. Ausgabe der Liste DONE
	3. Eingabe einer beliebigen Suchzahl zwischen 1 und 9 durch den Benutzer. DONE
	4. Ausgabe wie oft sich die Zahl in der Liste befindet. DONE
	5. Ausgabe aller Indices, an denen die Suchzahl auftritt. DONE
	6. Löschen aller Elemente der Liste, die die Suchzahl enthalten. DONE
	7. Ausgabe der Liste DONE
	8. Einfügen einer 0 als neues Element hinter jeder 5 in der Liste. DONE
	9. Ausgabe der Liste. DONE */

		ArrayList<Integer> myList = new ArrayList<Integer>();
		Scanner tastatur = new Scanner(System.in);
		int such;
		int zahl = 0;
		
		for(int i = 0; i<20; i++) {
			myList.add((int) (Math.random()*8+1));
		}
		//System.out.println(myList.size());
		
		printList(myList);
		do{
			System.out.println("Bitte geben sie eine Suchzahl zwischen 1 und 9 ein. ");
			such = tastatur.nextInt();
		}
		while(such < 1||such > 9);
		System.out.print("Die gesuchte Zahl befindet sichan den Stellen: ");
		for(int i = 0; i<myList.size(); i++) {
			if(myList.get(i) == such) {
				System.out.print(i+", ");
				zahl++;
			}
		}
		System.out.println("\nDie gesuchte Zahl ist "+zahl+" mal in der Liste vorhanden.");
		for(int i = 0; i<myList.size(); i++) {
			if(myList.get(i) == such) {
				myList.remove(i);
				i--;
			}
		}
		printList(myList);
		System.out.println("Wollen sie an jede 0 eine 5 anfuegen? (True || False)");
		boolean zero = tastatur.nextBoolean();
		if(zero == true) {
			for(int i = 0; i<myList.size(); i++) {
				if(myList.get(i) == 5) {
					i++;
					myList.add(i, 0);
				}
			}
		}
		printList(myList);
	}
	private static void printList(ArrayList<Integer> myList) {
		for(int i = 0; i<myList.size(); i++) {
			System.out.printf("myList [%2d] : %d\n", i, myList.get(i));
		}
	}
}
