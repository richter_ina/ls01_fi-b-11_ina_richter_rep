public class Arrays {
	
	public static void main(String[] args) {
		
		int lottoZahlen[] = {3, 7, 12, 18, 37, 42};
		String loesung = "";
		String wahrheit12 = "nicht ";
		String wahrheit13 = "nicht ";
		for(int i = 0; i < lottoZahlen.length; i++) {
			 loesung = loesung + lottoZahlen[i] + " ";
			 if(lottoZahlen[i] == 12) {
				 wahrheit12 = "";
			 }
			 else if(lottoZahlen[i] == 13) {
				 wahrheit13 = "";
			 }
		};
		System.out.println("[ "+ loesung +"]\n");
		System.out.println("Die Zahl 12 ist "+ wahrheit12 +"in der Ziehung enthalten.");
		System.out.println("Die Zahl 13 ist "+ wahrheit13 +"in der Ziehung enthalten.");
	}
}
