import java.util.Scanner;

public class MittelwertMitSchleife {

	public static void main(String[] args) {
		// (E) "Eingabe"
		// Werte für x und y festlegen:
		// ===========================
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Von wie vielen Zahlen soll der Mittelwert berechnet werden?");
		int array[] = new int[tastatur.nextInt()];
		double summe = 0;
				
		// (V) Verarbeitung
		// Mittelwert von x und y berechnen:
		// ================================
		for(int i = 0; i < array.length; i++) {
			System.out.println("Bitte gebe eine Zahl für die Mittelwertberechnung ein: ");
			array[i] = tastatur.nextInt();
			summe = summe + array[i];
		}
		double mittelwert = summe /array.length;

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.print("Der Mittelwert lautet: " + mittelwert);
				

	}

}
