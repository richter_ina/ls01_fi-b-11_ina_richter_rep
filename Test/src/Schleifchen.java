import java.util.Scanner;

public class Schleifchen {
	
	public static void star(int laenge, String zeichen) {
		
		for(int i = laenge; i > 0; i--) {
			
			System.out.print(zeichen);
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner tastatur = new Scanner(System.in);
		
		//Quadrat mit for-Schleife
		System.out.print("Gewünschte Seitenlänge des Quadrates: ");
		int seite = tastatur.nextInt();
		int space = seite-2;
		star(seite, "* ");
		System.out.printf("\n");
		
		for(int i = space; i > 0; i--) {
			
			System.out.print("* ");
			star(space, ". ");
			System.out.println("*");
		}
		star(seite, "* ");
		
		//Temperaturumrechnung mit while-Schleife
		System.out.print("Bitte den Startwert in Grad Celcius eingeben: ");
		double temp = tastatur.nextInt();
		System.out.print("Bitte den Endwert in Grad Celcius eingeben: ");
		double ende = tastatur.nextInt();
		System.out.print("Bitte die Schrittweite in Grad Celcius eingeben: ");
		double schrittweite = tastatur.nextInt();
		
		while(temp <= ende) {
			
			double ami = (temp * 9/5) + 32;
			System.out.printf("%6.2f ˆC |",temp);
			System.out.printf("%6.2f ˆF\n",ami);
			temp+=schrittweite;
		}
		
		//Matrix mit do-while-Schleife
		System.out.print("Bitte geben Sie eine Zahl zwischen 2 und 9 ein: ");
		int zahl = tastatur.nextInt();
		if(zahl<2||zahl>9) {
			
			System.out.print("Ob du lesen kannst? Bitte starte das Programm erneut.");
		}
		
		int numb = 0;
		do {
			int ten = numb/10;
			int one = numb-(ten*10);
			if(numb==0) {
				System.out.print(numb+"  ");
				numb++;
				continue;
			}
			if(numb%zahl==0||ten+one==zahl||one==zahl||ten==zahl) {
				
				System.out.print("*  ");
			}
			else {
				if(ten==0) {
					System.out.print(numb+"  ");
				}
				else {
					System.out.print(numb+" ");
				}
			}
			numb++;
			if(one==9) {
				
				System.out.printf("\n");
			}
		}
		while(numb!=100);
	}

}
