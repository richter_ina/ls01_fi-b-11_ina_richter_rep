import java.util.Scanner;

public class KL2 {
	
	private static void programmhinweis() {
		
		System.out.println("Hinweis: ");
        System.out.println("Das Programm multipliziert 2 eingegebene Zahlen. ");
        
	}
	
	private static void ausgabe(double z1, double z2, double erg) {
		
		System.out.println("Ergebnis der Multiplikation: ");
        System.out.printf("%.2f * %.2f = %.2f%n", z1, z2, erg);
		
	}
	
	private static double verarbeitung(double z1, double z2) {
		
		double erg = z1 * z2;
		return erg;
		
	}
	
	private static double eingabe1(double z1, Scanner myScanner) {
		
        System.out.print(" 1. Zahl: ");
        z1 = myScanner.nextDouble();
        return z1;
	}
	
	
	
	private static double eingabe2(double z2, Scanner myScanner) {
		
        System.out.print(" 2. Zahl: ");
        z2 = myScanner.nextDouble();
        return z2;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double zahl1 = 0.0, zahl2 = 0.0, erg = 0.0;

        Scanner myScanner = new Scanner(System.in);
       
        //1.Programmhinweis
        programmhinweis();
        
        //4.Eingabe
        zahl1 = eingabe1(zahl1, myScanner);
        zahl2 = eingabe2(zahl2, myScanner);

        //3.Verarbeitung
        erg = verarbeitung(zahl1, zahl2);

        //2.Ausgabe   
        ausgabe(zahl1, zahl2, erg);
		
	}

}
