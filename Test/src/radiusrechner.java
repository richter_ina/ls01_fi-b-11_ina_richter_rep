import java.util.Scanner;

public class radiusrechner {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		double a = 4;
		double b = 3;
		final double PI = 3.14;
		
		Scanner myScanner = new Scanner(System.in);
		
		// get radius
		System.out.println("Der Radius der Kugel in m beträgt: ");
		double r = myScanner.nextDouble();
		
		// formelrechnung
		double c = (a/b)-1;
		double erg = c * PI * (r*r*r);
		
		// ausgabe
		System.out.println("Das Volumen der Kugel in mˆ3 beträgt: " + erg);
	}

}
