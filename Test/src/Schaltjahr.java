import java.util.Scanner;

public class Schaltjahr {
	
	public static boolean ruleOne(int year) {
		
		boolean truth = (year%4 == 0);
		return truth;
	}
	
	public static boolean allRule(int year) {
		
		boolean truth = ruleOne(year);
		if (truth == true) {
			
			boolean jahrhundert = (year%100 == 0);
			if (jahrhundert == true && (year%400) != 0) {
				
				truth = false;
			}
		}
		return truth;
	}

	public static void main(String[] args) {
		
		Scanner tastatur = new Scanner(System.in);
	    System.out.print("Welches Jahr möchten Sie abfragen? ");
		int year = tastatur.nextInt();
		
		boolean truth = false;
		if (year < -45) {
			
			System.out.print("Schaltjahre wurden erst 45 v. Chr. eingeführt.");
			System.exit(0);
		}
		else {
			if (year < 1582) {
			
				truth = ruleOne(year);
			}
			else {
			
				truth = allRule(year);
			}
		}
		
		System.out.print("Das abgefragte Jahr ist ein Schaltjahr. " + truth);
	}

}
