import java.util.Scanner;

public class romanNumb {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner tastatur = new Scanner(System.in);
		System.out.print("Bitte römisches Zahlenzeichen eingeben. ");
		char zeichen = tastatur.next().charAt(0);
		
		switch(zeichen) {
		
		case 'I':
			System.out.println("1");
			break;
		case 'V':
			System.out.println("5");
			break;
		case 'X':
			System.out.println("10");
			break;
		case 'L':
			System.out.println("50");
			break;
		case 'C':
			System.out.println("100");
			break;
		case 'D':
			System.out.println("500");
			break;
		case 'M':
			System.out.println("1000");
			break;
		default:
			System.out.println("Dieses Zeichen ist kein römisches Zeichen. Bitte geben Sie nur ein einzelnes Zeichen als Großbuchstaben ein.");
			break;
		}
	}

}
