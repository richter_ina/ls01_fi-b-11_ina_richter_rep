
public class Temperaturtabelle {

	public static void main(String[] args) {
		
		// Aufgabe 1
		
		System.out.printf("%4s\n", "**");
		System.out.printf("%-1s", "*");
		System.out.printf("%5s\n", "*");
		System.out.printf("%-1s", "*");
		System.out.printf("%5s\n", "*");
		System.out.printf("%4s\n", "**");
		
		// Aufgabe 2
		
		System.out.printf("%-5s", "0!");
		System.out.printf("%1s", "= ");
		System.out.printf("%-19s", "");
		System.out.printf("%1s", "=");
		System.out.printf("%4s\n", "1");
		
		System.out.printf("%-5s", "1!");
		System.out.printf("%1s", "= ");
		System.out.printf("%-19s", "1");
		System.out.printf("%1s", "=");
		System.out.printf("%4s\n", "1");
		
		System.out.printf("%-5s", "2!");
		System.out.printf("%1s", "= ");
		System.out.printf("%-19s", "1 * 2");
		System.out.printf("%1s", "=");
		System.out.printf("%4s\n", "2");

		System.out.printf("%-5s", "3!");
		System.out.printf("%1s", "= ");
		System.out.printf("%-19s", "1 * 2 * 3");
		System.out.printf("%1s", "=");
		System.out.printf("%4s\n", "6");

		System.out.printf("%-5s", "4!");
		System.out.printf("%1s", "= ");
		System.out.printf("%-19s", "1 * 2 * 3 *4");
		System.out.printf("%1s", "=");
		System.out.printf("%4s\n", "24");

		System.out.printf("%-5s", "5!");
		System.out.printf("%1s", "= ");
		System.out.printf("%-19s", "1 * 2 * 3 * 4 * 5");
		System.out.printf("%1s", "=");
		System.out.printf("%4s\n", "120");
		
		// Aufgabe 3
		
		System.out.printf("%-20s", "Fahrenheit");
		System.out.printf("%1s", "|");
		System.out.printf("%20s\n", "Celcius");
		
		System.out.printf("%41s\n", "- - - - - - - - - - - - - - - - - - - - - ");

		System.out.printf("%+-20d", -20);
		System.out.printf("%1s", "|");
		System.out.printf("%20.2f\n", -28.8889);

		System.out.printf("%+-20d", -10);
		System.out.printf("%1s", "|");
		System.out.printf("%20.2f\n", -23.3333);

		System.out.printf("%+-20d", 0);
		System.out.printf("%1s", "|");
		System.out.printf("%20.2f\n", -17.7778);

		System.out.printf("%+-20d", 20);
		System.out.printf("%1s", "|");
		System.out.printf("%20.2f\n", -6.6667);

		System.out.printf("%+-20d", 30);
		System.out.printf("%1s", "|");
		System.out.printf("%20.2f\n", -1.1111);

	}

}
