import java.util.Scanner; // Import der Klasse Scanner
public class Eingabe {

	public static void main(String[] args) {
		// Neues Scanner-Objekt myScanner wird erstellt
		 Scanner myScanner = new Scanner(System.in);

		 System.out.print("Bitte geben Sie ihren Namen ein: ");
		 
		 String name = myScanner.nextLine();
		 
		 System.out.println(name);

		 float zahl1, zahl2;
		 double d;
		 long l;
		 int i;
		 short s;
		 byte b;
		 boolean tf;
		 char c;
		 
		 System.out.print("Bitte geben Sie eine Gleitkommazahl ein: ");

		 // Die Variable zahl1 speichert die erste Eingabe
		 zahl1 = myScanner.nextFloat();

		 System.out.print("Bitte geben Sie eine zweite Gleitkommazahl ein: ");

		 // Die Variable zahl2 speichert die zweite Eingabe
		 zahl2 = myScanner.nextFloat();

		 // Die Addition der Variablen zahl1 und zahl2
		 // wird der Variable ergebnis zugewiesen.
		 float ergebnis = zahl1 + zahl2;

		 System.out.println("\n\n\nErgebnis der Addition lautet: ");
		 System.out.println(zahl1 + " + " + zahl2 + " = " + ergebnis);
		 
		 System.out.print("Double: ");
		 d = myScanner.nextDouble();
		 System.out.println(d);
		 
		 System.out.print("Long: ");
		 l = myScanner.nextLong();
		 System.out.println(l);
		 
		 System.out.print("Integer: ");
		 i = myScanner.nextInt();
		 System.out.println(i);
		 
		 System.out.print("Short: ");
		 s = myScanner.nextShort();
		 System.out.println(s);
		 
		 System.out.print("Byte: ");
		 b = myScanner.nextByte();
		 System.out.println(b);
		 
		 System.out.print("Boolean: ");
		 tf = myScanner.nextBoolean();
		 System.out.println(b);
		 
		 System.out.print("Char: ");
		 c = myScanner.next().charAt(0);
		 System.out.println(c);
		 
		 myScanner.close();
		 
	}

}
