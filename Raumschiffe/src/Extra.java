
public class Extra {

	public static void main(String[] args) {
		Raumschiff klingonen = new Raumschiff( 1, 100, 100, 100, 100, 2, "IKS Hegh'ta");
		Raumschiff romulaner = new Raumschiff( 2, 100, 100, 100, 100, 2, "IRW Khazara");
		Raumschiff vulkanier = new Raumschiff( 0, 80, 80, 50, 100, 5, "Ni'Var");
		
		Ladung one = new Ladung(5, "Ferengi Schneckensaft");
		Ladung two = new Ladung(5, "Borg-Schrott");
		Ladung three = new Ladung(2, "Rote Materie");
		Ladung four = new Ladung(35, "Forschungssonden");
		Ladung five = new Ladung(200, "Bat'leth Klingonen Schwert");
		Ladung six = new Ladung(50, "Plasma-Waffe");
		Ladung seven = new Ladung(3, "Photonentorpedo");

		
		klingonen.addLadung(one);
		klingonen.addLadung(five);
		
		romulaner.addLadung(six);
		romulaner.addLadung(two);
		romulaner.addLadung(three);
		
		vulkanier.addLadung(four);
		vulkanier.addLadung(seven);
		
		klingonen.photonentorpedoSchuss(romulaner);
		romulaner.phaserkanoneSchuss(klingonen);
		vulkanier.nachricht("Gewalt ist nicht logisch.");
		klingonen.zustand();
		klingonen.ladungsverzeichnis();
		vulkanier.zustand();
		vulkanier.reparieren(true,true,true,true,vulkanier.getAndroidenAnzahl());
		vulkanier.nachladen(seven.getAnzahl());
		vulkanier.zustand();
		vulkanier.fegen();
		vulkanier.logbuch();
	}

}
