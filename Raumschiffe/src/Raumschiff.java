import java.util.ArrayList;

public class Raumschiff {
	//Attribute
	private int photonentorpedoAnzahl;
	private int energieversorgungInProzent;
	private int schildeInProzent;
	private int huelleInProzent;
	private int lebenserhaltungssystemeInProzent;
	private int androidenAnzahl;
	private String schiffsname;
	public static ArrayList<String> broadcastKommunikator = new ArrayList<String>();
	private ArrayList<Ladung> ladungsverzeichnis = new ArrayList<Ladung>();
	
	//Konstruktoren
	public Raumschiff() {
		
	}
	public Raumschiff( int photonentorpedoAnzahl, int energieversorgungInProzent, int schildeInProzent, int huelleInProzent, int lebenserhaltungssystemeInProzent, int androidenAnzahl, String schiffsname) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
		this.energieversorgungInProzent = energieversorgungInProzent;
		this.schildeInProzent = schildeInProzent;
		this.huelleInProzent = huelleInProzent;
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
		this.androidenAnzahl = androidenAnzahl;
		this.schiffsname = schiffsname;
	}
	
	//getter
	public int getPhotonentorpedoAnzahl() {
		return this.photonentorpedoAnzahl;
	}
	public int getEnergieversorgungInProzent() {
		return this.energieversorgungInProzent;
	}
	public int getSchildeInProzent() {
		return this.schildeInProzent;
	}
	public int getHuelleInProzent() {
		return this.huelleInProzent;
	}
	public int getLebenserhaltungssystemeInProzent() {
		return this.lebenserhaltungssystemeInProzent;
	}
	public int getAndroidenAnzahl() {
		return this.androidenAnzahl;
	}
	public String getSchiffsname() {
		return this.schiffsname;
	}
	
	//setter
	public void setPhotonentorpedoAnzahl(int photonentorpedoAnzahl) {
		this.photonentorpedoAnzahl = photonentorpedoAnzahl;
	}
	public void setEnergieversorgungInProzent(int energieversorgungInProzent) {
		this.energieversorgungInProzent = energieversorgungInProzent;
	}
	public void setSchildeInProzent(int schildeInProzent) {
		this.schildeInProzent = schildeInProzent;
	}
	public void setHuelleInProzent(int huelleInProzent) {
		this.huelleInProzent = huelleInProzent;
	}
	public void setLebenserhaltungssystemeInProzent(int lebenserhaltungssystemeInProzent) {
		this.lebenserhaltungssystemeInProzent = lebenserhaltungssystemeInProzent;
	}
	public void setAndroidenAnzahl(int androidenAnzahl) {
		this.androidenAnzahl = androidenAnzahl;
	}
	public void setSchiffsname(String schiffsname) {
		this.schiffsname = schiffsname;
	}

	
	public void addLadung(Ladung Ladung) {
		ladungsverzeichnis.add(Ladung);
	}
	
	
	public void zustand() {
		System.out.println("schiffsname:"+this.schiffsname);
		System.out.println("photonentorpedoAnzahl:"+this.photonentorpedoAnzahl);
		System.out.println("energieversorgungInProzent:"+this.energieversorgungInProzent);
		System.out.println("schildeInProzent:"+this.schildeInProzent);
		System.out.println("huelleInProzent:"+this.huelleInProzent);
		System.out.println("lebenserhaltungssystemeInProzent:"+this.lebenserhaltungssystemeInProzent);
		System.out.println("androidenAnzahl:"+this.androidenAnzahl);
	}
	
	public void ladungsverzeichnis() {
		for(int i = 0; i < ladungsverzeichnis.size(); i++) {
			System.out.println(ladungsverzeichnis.get(i));
		}
	}
	
	public void photonentorpedoSchuss(Raumschiff ziel) {
		if (photonentorpedoAnzahl > 0) {
			setPhotonentorpedoAnzahl(photonentorpedoAnzahl-1);
			nachricht("Photonentorpedo abgeschossen.");
			treffer(ziel);
		}
		else {
			nachricht("-=*Click*=-");
		}
	}
	
	public void phaserkanoneSchuss(Raumschiff ziel) {
		if (energieversorgungInProzent > 50) {
			setEnergieversorgungInProzent(energieversorgungInProzent-50);
			nachricht("Phaserkanone abgeschossen.");
			treffer(ziel);
		}
		else {
			nachricht("-=*Click*=-");
		}
	}
	
	public void treffer(Raumschiff ziel) {
		System.out.println(ziel.schiffsname+" wurde getroffen!");
		setSchildeInProzent(ziel.schildeInProzent-50);
		if (ziel.schildeInProzent <= 50) {
			setHuelleInProzent(ziel.huelleInProzent-50);
			setEnergieversorgungInProzent(ziel.energieversorgungInProzent-50);
			if (ziel.huelleInProzent <= 0) {
				setLebenserhaltungssystemeInProzent(0);
				nachricht("Lebenserhaltungssysteme sind vernichtet worden.");
			}
		}
	}
	
	public void nachricht(String nachricht) {
		broadcastKommunikator.add(nachricht);
	}
	
	public void logbuch() {
		for(int i = 0; i < broadcastKommunikator.size(); i++) {
			System.out.println(broadcastKommunikator.get(i));
		}
	}
	
	public void nachladen(int menge) {
		int l8 = 0;
		int vorher = photonentorpedoAnzahl;
		for(int i = 0; i < ladungsverzeichnis.size(); i++) {
			if (ladungsverzeichnis.get(i).getName() == "Photonentorpedo") {
				l8 = ladungsverzeichnis.get(i).getAnzahl();
			}
		}
		if (l8 == 0) {
			System.out.println("Keine Photonentorpedos gefunden!");
			nachricht("-=*Click*=-");
		}
		else if (menge >= l8) {
			setPhotonentorpedoAnzahl(photonentorpedoAnzahl+l8);
			for(int i = 0; i < ladungsverzeichnis.size(); i++) {
				if (ladungsverzeichnis.get(i).getName() == "photonentorpedos" || ladungsverzeichnis.get(i).getName() == "Photonentorpedos") {
					ladungsverzeichnis.get(i).setAnzahl(0);
				}
			}
			System.out.println((photonentorpedoAnzahl-vorher)+" Photonentorpedo(s) eingesetzt.");
		}
		else {
			setPhotonentorpedoAnzahl(photonentorpedoAnzahl+menge);
			for(int i = 0; i < ladungsverzeichnis.size(); i++) {
				if (ladungsverzeichnis.get(i).getName() == "photonentorpedos" || ladungsverzeichnis.get(i).getName() == "Photonentorpedos") {
					ladungsverzeichnis.get(i).setAnzahl(l8-menge);
				}
			}
			System.out.println((photonentorpedoAnzahl-vorher)+" Photonentorpedo(s) eingesetzt.");
		}
	}
	
	public void fegen() {
		for(int i = ladungsverzeichnis.size()-1; i >= 0; i--) {
			if (ladungsverzeichnis.get(i).getAnzahl() == 0) {
				ladungsverzeichnis.remove(i);
			}
		}
	}
	
	public void reparieren(boolean schild, boolean huelle, boolean lebenserhaltungssysteme, boolean energieversorgung, int droiden) {
		if (droiden > androidenAnzahl) {
			droiden = androidenAnzahl;
		}
		int masse = 0;
		if (schild == true) {
			masse++;
		}
		if (huelle == true) {
			masse++;
		}
		if (lebenserhaltungssysteme == true) {
			masse++;
		}
		if (energieversorgung == true) {
			masse++;
		}
		
		int e = ((int)(Math.random()*100))*droiden/masse;
		if (schild == true) {
			this.setSchildeInProzent(this.schildeInProzent+e);
		}
		if (huelle == true) {
			setHuelleInProzent(huelleInProzent+e);
		}
		if (lebenserhaltungssysteme == true) {
			setLebenserhaltungssystemeInProzent(lebenserhaltungssystemeInProzent+e);
		}
		if (energieversorgung == true) {
			setEnergieversorgungInProzent(energieversorgungInProzent+e);
		}
	}
}
