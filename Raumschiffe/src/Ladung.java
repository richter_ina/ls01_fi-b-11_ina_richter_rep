
public class Ladung {
	//Attribute
	private String name;
	private int anzahl;
	
	
	//Konstruktoren
	public Ladung() {
		
		}
	public Ladung(int anzahl, String name) {
		this.anzahl = anzahl;
		this.name = name;
	}
	
	
	//getter
	public String getName() {
		return this.name;
	}
	public int getAnzahl() {
		return this.anzahl;
	}
	
	//setter
	public void setName(String name) {
		this.name = name;
	}
	public void setAnzahl(int anzahl) {
		this.anzahl = anzahl;
	}
	
	@Override
	public String toString() {
		return name+" "+anzahl;
	}
}
