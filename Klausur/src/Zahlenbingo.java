import java.util.Scanner;

public class Zahlenbingo {

	public static void main(String[] args) {
		boolean gleich = false;
		Scanner tastatur = new Scanner(System.in);
		int a = 0;
		int b = 0;
		do{
			System.out.print("Geben Sie bitte die erste Zahl ein: ");
			a = tastatur.nextInt();
			System.out.print("Geben Sie bitte die zweite Zahl ein: ");
			b = tastatur.nextInt();
			
			if(a == b) {
				System.out.println("Die eingegebenen Zahlen sind gleich groß.");
				continue;
			}
			if(a > b) {
				int z = a;
				a = b;
				b = z;
				gleich = true;
			}
		}
		while(gleich == false);
		
		System.out.println("Die ungeraden Zahlen zw. din eingegebenen sind:");
		int g = 0;
		int y = 0;
		for(int i = a+1; i < b; i++) {
			if(i % 2 != 0) {
				System.out.println(i);
				y++;
			}
			else {
				g++;
			}
		}
		System.out.println("Die Anzahl der geraden zahlen ist: "+g);
		System.out.println("Die Anzahl der ungeraden zahlen ist: "+y);
		tastatur.close();
	}

}
