﻿import java.util.Scanner;

class Fahrkartenautomat
{	
	public static int fahrkartenVorbestellung(Scanner d) {
		
		boolean zeit = true;
		int wahl = 1;
		while(zeit) {
			System.out.print("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:\nEinzelfahrschein Regeltarif AB [2,90 EUR] (1)\nTageskarte Regeltarif AB [8,60 EUR] (2)\nKleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)\nBezahlen (9)\nIhre Wahl: ");
			wahl = d.nextInt();
			if(wahl == 1||wahl == 2||wahl == 3||wahl == 9) {
				zeit = false;
			}
			else {
				System.out.println("\nLernen Sie bitte lesen, oder setzen Sie ihre Brille auf!\n");
			}
		}
		return wahl;
	}
	
	public static int fahrkartenanzahlErfassen(Scanner c) {
	    
		boolean zeit = true;
		int anzahl = 1;
		while(zeit) {
			System.out.print("Anzahl der zu kaufenden Tickets: ");
			anzahl = c.nextInt();
			if (anzahl > 10 || anzahl < 1) {
				System.out.println("Ihre Bestellung enthielt eine ungültige Anzahl Tickets. Bitte geben Sie eine Anzahl zwischen 1 und 10 ein.");
			}
			else {
				zeit = false;
			}
		}
	    return anzahl;
	}
	
	public static float fahrkartenbestellungErfassen(Scanner a, int anzahl, int wahl) {
	       
		boolean zeit = true;
	    /*System.out.print("Zu zahlender Betrag (EURO): ");
	    float preis = a.nextFloat();
	    if (preis < 0) {
	    	preis = 1;
	    	System.out.println("Der angegebene Preis war negativ, Sie bekommen aber kein Geld für einen Ticketkauf. Es wird 1 EURO berechnet.");
	    }*/
		float preis = (float) 2.9;
		switch(wahl) {
		case 1:
			preis = (float) 2.90;
			break;
		case 2:
			preis = (float) 8.60;
			break;
		case 3:
			preis = (float) 23.50;
			break;
		}
	    return preis * anzahl;
	}
	
	public static float fahrkartenBezahlen(Scanner b,float zuZahlenderBetrag) {
		
		float eingezahlterGesamtbetrag = 0.0f;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   float difference = zuZahlenderBetrag - eingezahlterGesamtbetrag;
	    	   System.out.printf("Noch zu zahlen: %.2f EURO\n", difference);
	    	   System.out.print("Eingabe (mind. 5Ct, höchstens 99 Euro): ");
	    	   float eingeworfeneMünze = b.nextFloat();
	           eingezahlterGesamtbetrag += eingeworfeneMünze; 
	       }
	        
	    return eingezahlterGesamtbetrag - zuZahlenderBetrag;
	}
	
	public static void fahrkartenAusgeben() {
		
		System.out.println("\nFahrschein wird ausgegeben");
		warte(25);
	    System.out.println("\n\n");
	}
	
	public static void warte(int millisekunde) {
	
		for (int i = millisekunde; i != 0; i--) {
	          System.out.print("=");
	          try {
				Thread.sleep(50);
			  }
	          catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			  }
		}
	}
	
	public static void rueckgeldAusgeben(float rückgabebetrag){
		
		if(rückgabebetrag > 0.00)
	       {
	    	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO\n", rückgabebetrag);
	    	   System.out.println("wird in folgenden Münzen ausgezahlt:");

	           while(rückgabebetrag >= 2.0) // 2 EURO-Münzen
	           {
	        	  System.out.println("2 EURO");
		          rückgabebetrag -= 2.0;
	           }
	           while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
	           {
	        	  System.out.println("1 EURO");
		          rückgabebetrag -= 1.0;
	           }
	           while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
	           {
	        	  System.out.println("50 CENT");
		          rückgabebetrag -= 0.5;
	           }
	           while(Math.round(rückgabebetrag * 100.0) / 100.0 >= 0.2) // 20 CENT-Münzen
	           {
	        	  System.out.println("20 CENT");
	 	          rückgabebetrag -= 0.2;
	           }
	           while(Math.round(rückgabebetrag * 100.0) / 100.0 >= 0.1) // 10 CENT-Münzen
	           {
	        	  System.out.println("10 CENT");
		          rückgabebetrag -= 0.1;
	           }
	           while(Math.round(rückgabebetrag * 100.0) / 100.0 >= 0.05)// 5 CENT-Münzen
	           {
	        	   muenzeAusgeben(0.05, "Cent");
			       rückgabebetrag -= 0.05;
	           }
	       }
	}
	
	public static void muenzeAusgeben(double betrag, String einheit) {
		
		System.out.println(betrag+" "+einheit);
	}
	
	public static void erinnerung() {
		
		System.out.println("\nVergessen Sie nicht, den Fahrschein vor Fahrtantritt zu entwerten!\nWir wünschen Ihnen eine gute Fahrt.\n");
	}
	
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
       boolean zeit = true;
       int anzahl;
       int wahl;
       boolean rechnung = true;
       float zwischenbetrag;
       float zuZahlenderBetrag = 0;
       while(zeit) {
    	   //Fahrkartenbestellvorgang
    	   //------------------------
    	   while(rechnung){
    		   wahl = fahrkartenVorbestellung(tastatur);
    		   if(wahl == 9) {
    			   rechnung = false;
    		   }
    		   else {
    			   anzahl = fahrkartenanzahlErfassen(tastatur);
    			   zwischenbetrag = fahrkartenbestellungErfassen(tastatur, anzahl, wahl);
    			   System.out.println("Ihr Zwischenbetrag: "+zwischenbetrag+"\n");
    			   zuZahlenderBetrag = zuZahlenderBetrag+zwischenbetrag;
    		   }
    		   
    	   }
    	   // Geldeinwurf
    	   // -----------
    	   float rückgabebetrag = fahrkartenBezahlen(tastatur, zuZahlenderBetrag);
       
    	   // Fahrscheinausgabe
    	   // -----------------
    	   fahrkartenAusgeben();
    	   rueckgeldAusgeben(rückgabebetrag);
    	   erinnerung();
       }
       tastatur.close();
    }
}