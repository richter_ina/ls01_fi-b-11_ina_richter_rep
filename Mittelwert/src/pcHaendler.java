import java.util.Scanner;

public class pcHaendler {
	
	public static String liesString(Scanner a) {
		
		System.out.println("Was möchten Sie bestellen?");
		a.close();
		return a.next();
	}
	
	public static int liesInt(Scanner b) {
		
		System.out.println("Geben Sie die Anzahl ein:");
		b.close();
		return b.nextInt();
	}
	
	public static double liesDouble(Scanner c, String text) {
		
		System.out.println(text);
		c.close();
		return c.nextDouble();
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double preis) {
		
		return anzahl * preis;
	}
	
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		
		return nettogesamtpreis * (1 + mwst / 100);
	}
	
	public static void rechungausgeben(String artikel, int anzahl, double nettogesamtpreis, double bruttogesamtpreis, double mwst) {
		
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
	}

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);

		// Benutzereingaben lesen
		String artikel = liesString(myScanner);

		int anzahl = liesInt(myScanner);

		double preis = liesDouble(myScanner, "Geben Sie den Nettopreis ein:");
		double mwst = liesDouble(myScanner, "Geben Sie den Mehrwertsteuersatz in Prozent ein:");

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben
		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	}

}
