import java.util.Scanner;
	
public class Mittelwert {
	
	public static double print(Scanner my, String number) {
		
		System.out.print("Bitte geben Sie die " + number + " Zahl ein: ");
		double zahl = my.nextDouble();
		return zahl;
	
	}
	
	public static double verarbeitung(double zahlx, double zahly) {
		
		return (zahlx + zahly)/ 2.0;
		
	}
	
	public static void ausgabe(double M) {
		
		System.out.println("Mittelwert: " + M);
	}
	
	public static void main(String[] args) {
		
		//Deklaration von Variablen
		double zahl1;
		double zahl2;
		double m;
					
		//Eingabe
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Dieses Programm berechnet den Mittelwert zweier Zahlen.");
		zahl1 = print(myScanner, "erste");
		zahl2 = print(myScanner, "zweite");
		myScanner.close();
		
		//Verarbeitung
		m = verarbeitung(zahl1,zahl2);
		
		//Ausgabe
		ausgabe(m);
			
	}

}
