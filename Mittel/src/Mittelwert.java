import java.util.Scanner;

public class Mittelwert {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		System.out.println("Dieses Programm berechnet den Mittelwert beliebig vieler Zahlen. Wie viele Zahlen möchten sie eingeben?");
		int n = myScanner.nextInt();
		double speicher = 0;
		
		for (int i = n; i > 0; i--) {
			
			double zahl = eingabe(myScanner, "Bitte geben Sie die nächste Zahl ein: ");
			speicher += zahl;
		}
		
		double m = mittelwertBerechnung(speicher, n);
		
		ausgabe(m);
		
		myScanner.close();
	}
	
	public static double eingabe(Scanner ms, String text ) {
		System.out.print(text);
		double zahl = ms.nextDouble();
		return zahl;
	}
	
	public static double mittelwertBerechnung(double speicher, int n) {
		double m = (speicher)/ n;
		return m;
	}
	
	public static void ausgabe(double mittelwert) {
		System.out.println("Der errechnete Mittelwert beider Zahlen ist: " + mittelwert);
	}
}